import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {UserService} from './shared/service/user.service'
import {HttpClientModule} from '@angular/common/http'
import {Configuration} from './app.configuration'
 


@NgModule({
  declarations: [
    AppComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [UserService, Configuration],
  bootstrap: [AppComponent]
})
export class AppModule { }
