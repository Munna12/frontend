import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
{
  path: '', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule),

},
{
  path: 'Home', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule), 

},
{
  path: 'UserList', loadChildren: () => import('./modules/userlist/userlist.module').then(m => m.UserlistModule), 

}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
