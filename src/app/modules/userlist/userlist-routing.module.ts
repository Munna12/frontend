import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserlistComponent } from '../../shared/components/userlist/userlist.component';
import { EdituserComponent } from '../../shared/components/userlist/edituser/edituser.component'

const routes: Routes = [
  {
    path: '', component: UserlistComponent
  },
  {
    path: 'editUser/:id', component: EdituserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserlistRoutingModule { }
