import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import {ReactiveFormsModule} from "@angular/forms";


import { UserlistRoutingModule } from './userlist-routing.module';
import { UserlistComponent } from '../../shared/components/userlist/userlist.component';
import { EdituserComponent } from '../../shared/components/userlist/edituser/edituser.component'

@NgModule({
  declarations: [UserlistComponent, EdituserComponent],
  imports: [
    CommonModule,
    UserlistRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UserlistModule { }
