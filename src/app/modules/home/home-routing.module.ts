import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../../shared/components/home/home.component';
import { AuthGuardGuard } from '../../shared/guard/auth-guard.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  // { path: 'Home', component: HomeComponent }
  // { path: 'Home', component: HomeComponent, canActivate: [AuthGuardGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
