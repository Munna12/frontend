import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from '../../shared/components/home/home.component';
import { CpDefaultThemeDirective } from '../../attribute-directives/cp-default-theme.directive';
import { CpCustomThemeDirective } from '../../attribute-directives/cp-custom-theme.directive';
import { CpColorDirective } from '../../attribute-directives/cp-color.directive';
import { DefaultColorOnEventDirective } from '../../attribute-directives/default-color-on-event.directive';
import { DynamicColorOnEventDirective } from '../../attribute-directives/dynamic-color-on-event.directive';
import {CpSampleDirective} from '../../attribute-directives/cp-sample.directive';
@NgModule({
  declarations: [HomeComponent,
    CpDefaultThemeDirective,
    CpCustomThemeDirective,
    CpColorDirective,
    DefaultColorOnEventDirective,
    DynamicColorOnEventDirective,
    CpSampleDirective],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule
  ]
})
export class HomeModule { }
