import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  tmpForm: FormGroup;
  submitted = false;
  txtsize = '25px';
  colors = ['CYAN', 'GREEN', 'RED'];
  myColor = '';
  titleColor = 'green';

  constructor(private fb: FormBuilder, private router: Router) {

    console.log("hi mkv");
  }

  ngOnInit() {
    this.createForm();
  }

  get f() { return this.tmpForm.controls; }

  createForm() {
    this.tmpForm = this.fb.group({
      username: ["", [Validators.required, Validators.minLength(6)]],
      password: ["", [Validators.required, Validators.minLength(8)]],

    });
  }

  save() {
    this.submitted = true;

    if (this.tmpForm.invalid) {
      return;
    }
    if (this.tmpForm.valid) {
      alert("hi login");
      this.router.navigate(['/UserList']);
      console.log("formvalue:", this.tmpForm);
    }
  }

}
