import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../service/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.sass']
})
export class EdituserComponent implements OnInit {
  tmpForm: FormGroup;
  submitted = false;
  userId = '';
  userData: any;
  // data: any;
  constructor(private Fb: FormBuilder, private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.createForm();

    this.userId = JSON.parse(this.route.snapshot.params.id);
    this.getUserData(this.userId);
  }

  get f() { return this.tmpForm.controls; }

  createForm() {
    this.tmpForm = this.Fb.group({
      username: ["", [Validators.required, Validators.minLength(6)]],
      lastname: ["", [Validators.required, Validators.minLength(8)]],
      firstname: ["", [Validators.required, Validators.minLength(8)]],
    });
  }

  getUserData(id) {
    this.userService.getUser(id).subscribe(
      (result: any) => {
        this.userData = result[0];
        this.tmpForm.patchValue({
          username: this.userData.username,
          lastname: this.userData.lastname,
          firstname: this.userData.firstname
        });
      },
      response => {
        console.log('error response:', response);
      }
    );
  }

  updateUser(){
    const payload = {
      'id': this.userId,
      'username': this.tmpForm.value.username,
      'firstname': this.tmpForm.value.firstname,
      'lastname': this.tmpForm.value.lastname,      
    }
    this.userService.updateUser(payload).subscribe((result: any) => {
      console.log("result", result);
    },
    response => {
      console.log('error response:', response);
    });

  }


}
