import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../../service/user.service';
import {Router} from '@angular/router'

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.sass']
})
export class UserlistComponent implements OnInit {
  submitted = false;
  user_list = [];
  isValidFormSubmitted = false;
  // tmpForm: FormGroup;
  constructor(public userservice: UserService, public router: Router) {
    console.log("hi userlist");
  }
  ngOnInit() {
    // this.createForm();
    this.GetList();
  }

  // get f() { return this.tmpForm.controls; }

  // createForm() {
  //   this.tmpForm = this.fb.group({
  //     username: ['', [Validators.required, Validators.maxLength(15)]],
  //     password: ['', [Validators.required, Validators.minLength(8)]]
  //   });
  // }

  onSubmitTemplateBased(form: NgForm) {
    this.isValidFormSubmitted = false;
    if (form.valid) {
      this.isValidFormSubmitted = true;
      console.log("template driven data:", form);
    } else {
      return;
    }
  }
  resetUserForm(form: NgForm) {
    form.resetForm();
  }

  //  save(){
  //   this.submitted = true;
  //   if (this.tmpForm.valid) {
  //   console.log("formvalue:", this.tmpForm);  
  //   }
  //  }



  GetList(){
    debugger;
    this.userservice.getUserList().subscribe(
      (result: any) => {
        console.log(result)
        this.user_list = result;
      },
      response => {
        console.log('error response:', response);
      }
    );
  }

  editUser(user) {
    this.router.navigate(['/UserList/editUser/'+`${user}`]);
  }
}
