import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from'@angular/common/http';

import {Configuration} from '../../app.configuration';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient, public config: Configuration) { }

  getUserList(){
    return this.http.get(this.config.UserAPI +'/user/userList', this.httpOptions);
  }

  getUser(id){
    return this.http.get(this.config.UserAPI +'/user/getuser/'+`${id}`, this.httpOptions);
  }

  updateUser(payload){
    return this.http.post(this.config.UserAPI+ '/user/update',payload, this.httpOptions);
  }

  
}
