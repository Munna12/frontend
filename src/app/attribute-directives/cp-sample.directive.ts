import { Directive, AfterViewInit, ElementRef } from '@angular/core';

@Directive({
  selector: '[appCpSample]'
})
export class CpSampleDirective implements AfterViewInit{

  constructor(private eleRef: ElementRef) { }
  ngAfterViewInit(): void {
    this.eleRef.nativeElement.style.color = 'blue'; 
    this.eleRef.nativeElement.style.fontSize = '20px';
 }
 
}
