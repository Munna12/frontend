import { Directive, ElementRef, Input, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appCpColor]'
})
export class CpColorDirective implements AfterViewInit {
  @Input() appCpColor: string;
  constructor(private elRef: ElementRef) {
  }
  ngAfterViewInit(): void {
    this.elRef.nativeElement.style.color = this.appCpColor;
  }
}
